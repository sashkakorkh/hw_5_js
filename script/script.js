/*Exercise 1*/

/*1.Метод об'єкту це функція, якій доступні лише значення в середині об'єкту або ті , до яких об'єкт має доступ
* 2. Як значення властивості об'єкту можуть бути я примітивні типи даних так і об'єкти(включаючи масиви, функції і ласне об'єкти).
* 3.Це значить що об'єкти зберігаються в сегменті пам'яті heap, а не stack як примітиви.
* Отже якщо ми матимемо const object = {}; то в імені констати буде зберігатись не об'єкт, а лиш посилання на нього */

/*Exercise 2*/

let userFirstName;
let userLastName;
let newUser;
function createNewUser () {
    checkUserInfo();
    newUser = {
        getLogin() {
            return this['first name'][0].toLowerCase() + this['last name'].toLowerCase()
        },
        setFirstName (newUserFirstName) {
        Object.defineProperty(newUser, 'first name', {value: newUserFirstName})
    },

        setLastName (newUserLastName) {
        Object.defineProperty(newUser, 'last name', {value: newUserLastName})
    }
    };
    Object.defineProperty(newUser, 'first name', {value: userFirstName, writable: false, configurable: true});
    Object.defineProperty(newUser, 'last name', {value: userLastName, writable: false, configurable: true});


    /*newUser['first name'] = userFirstName;
    newUser['last name'] = userLastName;*/
    return newUser;
}


function checkUserInfo () {
    userFirstName = prompt('Enter your name');
    while (!userFirstName || !isNaN(userFirstName) || (!/\S/.test(userFirstName))) {
        userFirstName = prompt('Enter your name correctly');
    }
    userLastName = prompt('Enter your last name');
    while (!userLastName || !isNaN(userLastName) || (!/\S/.test(userLastName))) {
        userLastName = prompt('Enter your last name correctly');
    }
}

let user = createNewUser()
user['first name'] = "Test 1";
user['last name'] = "Test 2";
console.log(user);

user.setFirstName('Test 1');
user.setLastName('Test 2');

console.log(createNewUser());
console.log(newUser.getLogin());

